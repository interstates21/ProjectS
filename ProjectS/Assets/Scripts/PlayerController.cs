﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerMotor))]
[RequireComponent(typeof(Animator))]
public class PlayerController : MonoBehaviour {

	[SerializeField]
	private float speed = 10f;
	[SerializeField]
	private float rotSpeed = 3f;
	private PlayerMotor motor;

	[SerializeField]
	private float thusterIntensity = 3000f;

    private Animator animator;

	void Start()
	{
		motor = GetComponent<PlayerMotor>();
        animator = GetComponent<Animator>();
	}

	void Update()
	{
		float _offsetX = Input.GetAxis("Horizontal"); // offsetx
		float _offsetZ = Input.GetAxis("Vertical"); // offsetz

		Vector3 _moveHorizontal = transform.right * _offsetX; // transorm.right = potential movement to the right
		Vector3 _moveVertical = transform.forward * _offsetZ; // position

		Vector3 _velosity = (_moveHorizontal + _moveVertical).normalized * speed; // movement matrix
		motor.Move(_velosity);

        animator.SetFloat("Blend", _offsetZ);
		float _yRot = Input.GetAxisRaw("Mouse X");
		Vector3 _rotation  = new Vector3 (0f, _yRot, 0f) * rotSpeed; //only turning

		float _xRot = Input.GetAxisRaw("Mouse Y");
		float _cameraRotation  = _xRot * rotSpeed; //only turning

		motor.Rotate(_rotation);
		motor.RotateCamera(_cameraRotation);

		// Jumping

		Vector3 _tForse = Vector3.zero;
		if (Input.GetButton ("Jump"))
		{
			_tForse = Vector3.up * thusterIntensity;
		}
		motor.ApplyThruster(_tForse);
	}
}
