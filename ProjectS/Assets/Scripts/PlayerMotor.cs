﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]

public class PlayerMotor : MonoBehaviour
{
	private Vector3 velocity = Vector3.zero;
	private Vector3 rotation = Vector3.zero;
	private float	cameraRotationX = 0;
	private float	currentCameraRotationX = 0;
	private Vector3 tForse = Vector3.zero;
	private Rigidbody rb;

	[SerializeField]
	private float cameraRotationLimit  = 85f;

	[SerializeField]
	private Camera cam;

	void Start()
	{
		rb = GetComponent<Rigidbody>();
	}

	public void Move(Vector3 _velocity)
	{
		velocity = _velocity;
	}

	public void Rotate(Vector3 _rotation)
	{
		rotation = _rotation;
	}

	public void RotateCamera(float _cameraRotation)
	{
		cameraRotationX = _cameraRotation;
	}


	public void ApplyThruster (Vector3 _tForse)
	{
		tForse = _tForse;
	}

	void FixedUpdate()
	{
		PerformMovement();
		PerformRotation();
	}

	void PerformMovement ()
	{
		if (velocity != Vector3.zero)
		{
			rb.MovePosition(rb.position + velocity * Time.fixedDeltaTime); // MoveToWhere?
		}
		if (tForse != Vector3.zero)
		{
			rb.AddForce (tForse * Time.fixedDeltaTime, ForceMode.Acceleration);
		}
	}

	void PerformRotation ()
	{
		rb.MoveRotation(rb.rotation * Quaternion.Euler(rotation)); // MoveToWhere?
		if (cam != null)
		{
			currentCameraRotationX -= cameraRotationX;
			currentCameraRotationX = Mathf.Clamp (currentCameraRotationX, -cameraRotationLimit, cameraRotationLimit);

			cam.transform.localEulerAngles = new Vector3 (currentCameraRotationX, 0f, 0f);
		}

	}

}
