﻿using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(Player))]
public class PlayerSetup : NetworkBehaviour
{
	[SerializeField]
	Behaviour[] componentsToDisable;


	[SerializeField]
	string remoteLayerName = "RemotePlayer";
	Camera sceneCamera;

    [SerializeField]
    string dontDrawLayerName = "DontDraw";
    [SerializeField]
    GameObject playerGraphics;
    void Start ()
	{
		if (!isLocalPlayer)
		{
			DisableComponents();
			AssignRemoteLayer();
		}
		else
		{
			sceneCamera = Camera.main;
			if (sceneCamera != null)
			{
				sceneCamera.gameObject.SetActive(false);
			}

            SetLayerRecursively(playerGraphics, LayerMask.NameToLayer(dontDrawLayerName));
		}

        GetComponent<Player>().Setup();
	}

    void SetLayerRecursively(GameObject o, int newLayer)
    {
        o.layer = newLayer;

        foreach (Transform child in o.transform)
        {
            SetLayerRecursively(child.gameObject, newLayer);
        }
    }


    void DisableComponents()
	{
		for (int i = 0; i < componentsToDisable.Length; i++)
			{
				componentsToDisable[i].enabled = false;
			}
	}

	void AssignRemoteLayer()
	{
		gameObject.layer = LayerMask.NameToLayer(remoteLayerName); //assign a layer to this
	}

    public override void OnStartClient() // It's already in the NetworkBehavior
    {
        base.OnStartClient();
        string _netID = GetComponent<NetworkIdentity>().netId.ToString();
        Player _player = GetComponent<Player>();

        GameManager.RegisterPlayer(_netID, _player);
    }

    void OnDisable ()
	{
		if (sceneCamera != null)
		{
			sceneCamera.gameObject.SetActive(true);
		}

        GameManager.UnRegisterPlayer(transform.name);
	}
}
